var margin = {top: 250, right: 40, bottom: 250, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var parse = d3.time.format("%b %Y").parse;

var x = d3.time.scale()
    .domain([new Date(2016, 0, 6, 8), new Date(2016, 0, 6, 10)])
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("top")
    .ticks(d3.time.hours, 3)
    .tickFormat(d3.time.format('%-I %p'))
    .tickSize(10,10);

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(5)
    .tickSize(-width);


var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right + 1)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + (margin.left) + "," + margin.top + ")");

var xAxisMinor = d3.svg.axis()
    .scale(x)
    .ticks(d3.time.hours, 1)
    .tickFormat('')
    .tickSize(-5, 10);



// background rectangle
svg.append("clipPath")
    .attr("id", "background-strip")
  .append("rect")
    .attr("class", "grid-background")
    .style("fill","lightgrey")
    .attr('x', 0)
    .attr('y', 0)
    .attr("width", width)
    .attr("height", 30);


svg.append("g")
    .attr("class", "x axis")
    .call(xAxis);

svg.append("g")
    .attr("class", "x axis minor")
    .call(xAxisMinor);


var dateFormat = d3.time.format("%Y-%m-%d");

// ********************
// add mouseover events
tooltip = d3.select("body")
   .append("div")
   .attr("class", "tooltip")
   .style("position", "absolute")
   .style("z-index", "10")
   .style("visibility", "hidden")
   .text("a simple tooltip");

function initTooltip() {
    d3.selectAll(".x.axis .tick")
        .on("mouseover", function () {
            // var text = d3.select(this).text();
            var date = d3.select(this).data()[0];
            var label = dateFormat(date);
            tooltip.style("top", (event.pageY - 30) + "px").style("left", (event.pageX - 20) + "px");
            tooltip.style("visibility", "visible");
            tooltip.text(label);
        })
        .on("mouseout", function () {
            tooltip.style("visibility", "hidden");
        });
}

initTooltip();

var color = d3.scale.category10();
var timeFormat = d3.time.format("%H:%m");

var xAxisValues = d3.svg.axis()
    .scale(x)
    .orient("top")
    .tickFormat(d3.time.format('%H:%M'))
    .tickSize(0, 0);


d3.csv('https://bitbucket.org/FMSurveyTeam/d3-test/raw/master/data.csv', function(d) {
  return {
    'Start Time': new Date(d['Start Time'].slice(0,-4)),
    'End Time': new Date(d['End Time'].slice(0,-4)),
    'Travel Mode': d['Travel Mode'] || 'Unknown'
  };
}, function(error, data) {
  // return;
  if (error) throw error;

  svg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("clip-path", "url(#background-strip)")
      .attr("class", function(d) {
        return d['Travel Mode'].toLowerCase();
      })
      .style("fill", function(d) {
        return color(d['Travel Mode']);
      })
      .attr("x", function(d) {
        // console.log(d['Start Time']);
        return x(d['Start Time']);
      })
      .attr("width", function(d) {
        return x(d['End Time']) - x(d['Start Time']);
      })
      .attr("y", function(d) { return 0; })
      .attr("height", function(d) { return 30; })
      .append("title")
      .text(function(d) {
        var mode = d['Travel Mode'] || 'Unknown';
        return mode + " (" + timeFormat(d['Start Time']) + ' to ' + timeFormat(d['End Time']) + ")";
      });

  xAxisValues.tickValues(data.map(function(d) {return d['Start Time']}));

  svg.append("g")
      .attr("class", "x axis values")
      .attr("transform", "translate(20," + 20 + ")")
      .call(xAxisValues);
});

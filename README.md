# Frontend Developer Technical Test

## Getting Started

This is a barebones setup for a simple working demo using d3. Fork the repository to work on the goals.


## Goals

Please develop a web-interface to present to users his mobility summary, which includes information on his activities and trips for a given day. The interface should consist of three components:

* A calendar for user to select the date that she wants to view.

* A timeline that summarized the day’s activities. Here is a mock up for this timeline.  
![Timeline](https://bitbucket.org/FMSurveyTeam/d3-test/raw/master/images/timeline.png)  
![Legend](https://bitbucket.org/FMSurveyTeam/d3-test/raw/master/images/legend.png)

* When user clicks on any part of the timeline, a closed-up view of that segment is shown, revealing more details for the selected hour.  
![Details](https://bitbucket.org/FMSurveyTeam/d3-test/raw/master/images/details.png)

## Bonus Goals

* Implement smooth navigation for data that stretches across longer periods of time (e.g. panning of timeline axis)
* Implement support for multiple timezones (e.g. using momentJS)
* Implement search/filtering functionality (e.g. display only trips using a `Car`, you may want to use a framework but do not spend excessive time on this)


## Deployment

For the working demo, you may consider the use of [Heroku](https://www.heroku.com),
[Github pages](https://pages.github.com), [Plunker](https://plnkr.co), [JSFiddle](https://jsfiddle.net) etc.

* Please use the private option for Plunker/JSFiddle.
* DO NOT spend excessive time on deployment.